/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  compiler: {
    styledComponents: true
  },
  images: {
    domains: ['sites.hostgrano.com.br', 'picsum.photos']
  }
}

module.exports = nextConfig
