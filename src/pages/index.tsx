import type { GetStaticProps, NextPage } from 'next'
import client from 'services/graphql'
import { GetPaginatedHomePageContentQuery } from 'services/graphql/generated/graphql'
import { GET_HOMEPAGE } from 'services/graphql/queries'
import Home, { HomeProps } from 'templates/Home'
import { homepageMapper } from 'utils'

const HomePage: NextPage<HomeProps> = ({ data, error }) => {
  return <Home data={data} error={error} />
}

export const getStaticProps: GetStaticProps = async () => {
  try {
    const response = await client.request<GetPaginatedHomePageContentQuery>(
      GET_HOMEPAGE
    )

    const data = homepageMapper(response)

    return {
      props: {
        data,
        error: false
      },
      revalidate: 5
    }
  } catch (error) {
    return {
      props: {
        posts: {},
        error: true
      },
      revalidate: 5
    }
  }
}

export default HomePage
