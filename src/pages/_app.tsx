import Header from 'components/common/Header'
import type { AppProps } from 'next/app'
import GlobalStyles from 'styles/global'
import '@fontsource/fira-sans/400.css'
import '@fontsource/fira-sans/700.css'
import Footer from 'components/common/Footer'

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <GlobalStyles />
      <Header />
      <Component {...pageProps} />
      <Footer />
    </>
  )
}

export default MyApp
