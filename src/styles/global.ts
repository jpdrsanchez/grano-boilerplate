import { createGlobalStyle } from 'styled-components'
import { reset, variables, wordpress } from './utils'

const GlobalStyles = createGlobalStyle`
  ${reset}
  ${wordpress}
  ${variables}

  html {
    box-sizing: border-box;
    font-size: 100%;
  }

  *,
  *::after,
  *::before {
    box-sizing: inherit;
  }

  body {
    font-family: var(--mainFont);
    background: var(--mainBg);
    color: var(--text);
  }

  a {
    color: var(--link);
  }
`

export default GlobalStyles
