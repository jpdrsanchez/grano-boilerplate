import axios from 'axios'
import { variables } from 'config'
import Errors from './constants/errors'

const http = axios.create({
  baseURL: variables.API_URL || ''
})

http.interceptors.response.use(
  response => response,
  error => {
    if (error.response.status === 400) {
      return {
        status: error.response.status,
        code: error.response.data.code,
        message: Errors[error.response.data.code as keyof typeof Errors] || ''
      }
    }

    return error
  }
)

export default http
