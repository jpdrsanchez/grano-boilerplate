import http from '..'

type PostsParams = {
  per_page?: number
  page?: number
  _fields?: string
  _embed?: number
  slug?: string
  exclude?: string
  categories?: string
  search?: string
  offset?: number
}

export const get = async (params: PostsParams) => {
  const {
    page = 1,
    per_page = 10,
    _embed = 1,
    _fields = 'id,slug,title,content,excerpt,date,_embedded,_links'
  } = params

  return http.get('/posts', {
    params: {
      page,
      per_page,
      _embed,
      _fields
    }
  })
}
