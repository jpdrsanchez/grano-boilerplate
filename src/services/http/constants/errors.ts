enum Errors {
  rest_invalid_param = 'Parâmetros de requisição inválidos',
  rest_post_invalid_page_number = 'A Página requisitada não existe'
}

export default Errors
