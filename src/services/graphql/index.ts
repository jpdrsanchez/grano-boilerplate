import { variables } from 'config'
import { GraphQLClient } from 'graphql-request'

const endpoint = variables.API_GRAPHQL || ''

const clientOptions: RequestInit = {}

const client = new GraphQLClient(endpoint, clientOptions)

export default client
