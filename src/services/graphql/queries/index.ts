import { gql } from 'graphql-request'

export const GET_HOMEPAGE = gql`
  query getPaginatedHomePageContent(
    $first: Int = 11
    $after: String = ""
    $before: String = ""
  ) {
    posts(first: $first, after: $after, before: $before) {
      pageInfo {
        startCursor
        endCursor
        hasNextPage
        hasPreviousPage
      }
      edges {
        cursor
        node {
          id
          title
          content
          slug
          date
          categories {
            edges {
              node {
                id
                slug
                name
              }
            }
          }
          featuredImage {
            node {
              sourceUrl
            }
          }
        }
      }
    }
  }
`
