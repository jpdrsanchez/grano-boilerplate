import { GetPaginatedHomePageContentQuery } from 'services/graphql/generated/graphql'

export const stripTags = (text: string) => text.replace(/(<([^>]+)>)/gi, '')

export const homepageMapper = (response: GetPaginatedHomePageContentQuery) => {
  const posts =
    response.posts?.edges?.map(post => ({
      id: post!.node!.id,
      title: post!.node!.title,
      content: post!.node!.content,
      image: post?.node?.featuredImage?.node?.sourceUrl || null,
      date: post!.node!.date,
      categories:
        post?.node?.categories?.edges?.map(category => ({
          id: category!.node!.id,
          slug: category!.node!.slug,
          name: category!.node!.name
        })) || []
    })) || []

  const pagination = response.posts?.pageInfo || null

  return {
    posts,
    pagination
  }
}
