import PostCard from 'components/PostCard'
import * as S from './styles'

type Post = {
  id: string
  slug: string
  title: string
  content: string
  image: string
  date: string
  categories: {
    id: string
    name: string
    slug: string
  }[]
}

type Pagination = {
  startCursor: string
  endCursor: string
  hasNextPage: string
  hasPreviousPage: string
}

export type HomeProps = {
  data: {
    posts: Post[]
    pagination: Pagination
  }
  error: boolean
}

const Home = ({ data, error }: HomeProps) => {
  if (error)
    return (
      <S.Wrapper>
        <S.EmptyOrError>Ocorreu um erro ao carregar a aplicação</S.EmptyOrError>
      </S.Wrapper>
    )

  const { posts, pagination } = data

  if (posts.length) {
    const [featuredPost] = posts

    return (
      <S.Wrapper>
        <S.Featured>
          <PostCard
            title={featuredPost.title}
            slug={featuredPost.slug}
            description={featuredPost.content}
            date={featuredPost.date}
            image={featuredPost.image}
            categories={featuredPost.categories}
          />
        </S.Featured>
        <S.PostsList>
          {posts.map(
            post =>
              post.id !== featuredPost.id && (
                <PostCard
                  key={post.id}
                  title={post.title}
                  slug={post.slug}
                  description={post.content}
                  date={post.date}
                  image={post.image}
                  categories={post.categories}
                />
              )
          )}
        </S.PostsList>
      </S.Wrapper>
    )
  }

  return (
    <S.Wrapper>
      <S.EmptyOrError>Parece que ainda não há nenhuma postagem</S.EmptyOrError>
    </S.Wrapper>
  )
}
export default Home
