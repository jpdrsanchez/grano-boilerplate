import Container from 'components/common/Container'
import styled from 'styled-components'

export const Wrapper = styled(Container)`
  padding-top: 2rem;
  padding-bottom: 4rem;
`

export const EmptyOrError = styled.div`
  height: calc(100vh - 16rem);
`

export const Featured = styled.main`
  margin-bottom: 3.75rem;
`

export const PostsList = styled.section`
  display: grid;
  gap: 3.75rem;

  @media (min-width: 48em) {
    grid-template-columns: 1fr 1fr;
  }
`
