import Container from 'components/common/Container'
import styled from 'styled-components'

export const Header = styled.header`
  padding-top: 32px;
  padding-bottom: 32px;

  h1 {
    color: var(--title);
    font-size: 2rem;
    font-weight: 700;
  }

  a {
    text-decoration: none;
  }
`

export const Wrapper = styled(Container)`
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-wrap: wrap;
  gap: 10px 30px;
`
