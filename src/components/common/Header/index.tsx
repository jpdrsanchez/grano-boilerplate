import * as S from './styles'

const Header = () => {
  return (
    <S.Header>
      <S.Wrapper>
        <h1>Grano Espresso</h1>
        <p>
          Boilerplate para blogs e sites{' '}
          <a
            href="https://granostudio.com.br/"
            target="_blank"
            rel="noreferrer"
          >
            Grano Studio
          </a>
        </p>
      </S.Wrapper>
    </S.Header>
  )
}
export default Header
