import styled from 'styled-components'

export const Footer = styled.footer`
  padding-bottom: 2rem;
  color: var(--title);
  font-size: 18px;
`

export const Logo = styled.div`
  display: flex;
  align-items: center;
  gap: 10px;

  & > * {
    &:first-child {
      flex: 0 0 40px;
    }
  }
`
