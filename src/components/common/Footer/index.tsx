import Image from 'next/image'
import Container from '../Container'
import * as S from './styles'

const Footer = () => {
  return (
    <S.Footer>
      <Container>
        <S.Logo>
          <Image src="/grano.png" alt="Grano" width={135} height={105} />
          <p>Grano Studio</p>
        </S.Logo>
      </Container>
    </S.Footer>
  )
}
export default Footer
