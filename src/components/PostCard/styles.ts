import styled from 'styled-components'

export const Wrapper = styled.a`
  display: block;
  position: relative;
  color: var(--text);
  text-decoration: none;

  h2 {
    color: var(--title);
    font-size: 24px;
    line-height: 1.4;
    font-weight: 700;
    margin-top: 10px;
    margin-bottom: 2px;
  }

  p {
    line-height: 1.4;
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-box-orient: vertical;
    -webkit-line-clamp: 3;
    margin-bottom: 15px;
  }

  time {
    font-size: 12px;
    margin-bottom: 15px;
    display: block;
  }

  ul {
    display: flex;
    align-items: center;
    gap: 10px;
    flex-wrap: wrap;
    font-size: 14px;
  }
`

export const CardThumb = styled.div`
  position: relative;
  width: 100%;
  aspect-ratio: 2 / 1;
  overflow: hidden;
  border-radius: 10px;
  box-shadow: 0 2px 10px rgba(14, 14, 14, 0.1);

  @media (min-width: 22.5em) {
    aspect-ratio: 5 / 2;
  }
`
