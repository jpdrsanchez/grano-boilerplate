import Image from 'next/image'
import Link from 'next/link'
import { stripTags } from 'utils'
import * as S from './styles'

type PostCardProps = {
  title: string
  slug: string
  description: string
  date: string
  image?: string
  categories?: {
    id: string
    slug: string
    name: string
  }[]
}

const PostCard = (props: PostCardProps) => {
  return (
    <Link href={`/posts/${props.slug}`} passHref>
      <S.Wrapper>
        <article>
          <S.CardThumb>
            <Image
              src={props.image || 'https://picsum.photos/1200'}
              alt={props.title}
              layout="fill"
              objectPosition="center"
              objectFit="cover"
            />
          </S.CardThumb>
          <h2>{props.title}</h2>
          <time dateTime={props.date}>
            <abbr aria-label="29 de Março de 2022">29/03/2022</abbr>
          </time>
          <p>{stripTags(props.description)}</p>
          {!!props.categories?.length && (
            <ul>
              {props.categories.map(
                category =>
                  category.slug !== 'sem-categoria' && (
                    <li key={category.id}>{category.name}</li>
                  )
              )}
            </ul>
          )}
        </article>
      </S.Wrapper>
    </Link>
  )
}

export default PostCard
