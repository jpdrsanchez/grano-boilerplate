export const variables = {
  API_URL: 'https://sites.hostgrano.com.br/grano-teste/wp-json/wp/v2/',
  API_GRAPHQL: 'https://sites.hostgrano.com.br/grano-teste/graphql'
}
